import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './libraries/guard/auth-guard.guard';
import { guestGuardGuard } from './libraries/guard/guest-guard.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full',
  },
  {
    path: 'sign-in',
    loadChildren: () =>
      import('@app/page/sign-in/sign-in.module').then((m) => m.SignInModule),
    canActivate: [guestGuardGuard],
  },
  {
    path: 'home',
    loadChildren: () =>
      import('@app/page/home/home.module').then((m) => m.HomeModule),
    canActivate: [authGuard],
  },
  {
    path: 'about',
    loadChildren: () =>
      import('@app/page/about/about.module').then((m) => m.AboutModule),
    canActivate: [authGuard],
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('@app/page/contact/contact.module').then((m) => m.ContactModule),
    canActivate: [authGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
