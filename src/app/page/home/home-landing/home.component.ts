import { Component } from '@angular/core';
import { TranslationService } from '@services/translate.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(private translationService: TranslationService) {}
  changeLanguage(lang: string): void {
    this.translationService.setLanguage(lang);
  }
}
