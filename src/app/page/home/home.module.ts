import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home-landing/home.component';
import { TranslatePipeModule } from '@app/libraries/pipe/translate/translate.pipe.module';

const routes: Routes = [{ path: '', component: HomeComponent }];

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, TranslatePipeModule, RouterModule.forChild(routes)],
})
export class HomeModule {}
