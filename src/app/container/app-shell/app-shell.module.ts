import { NgModule } from '@angular/core';
import { AppShellComponent } from './app-shell.component';
import { HeaderModule } from '@app/components/header/header.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AppShellComponent],
  exports: [AppShellComponent],
  imports: [CommonModule, HeaderModule],
})
export class AppShellModule {}
