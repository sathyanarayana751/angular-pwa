import { Component } from '@angular/core';
import { AuthService } from '@app/libraries/services/auth.service';

@Component({
  selector: 'app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss'],
})
export class AppShellComponent {
  constructor(protected auth: AuthService) {}
}
