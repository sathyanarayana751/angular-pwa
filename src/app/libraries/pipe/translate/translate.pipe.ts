import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '@services/translate.service';

@Pipe({
  name: 'translate',
  pure: false, // Set to false to make it update when language changes
})
export class TranslationPipe implements PipeTransform {
  constructor(private translationService: TranslationService) {}

  transform(key: string): string {
    return this.translationService.get(key);
  }
}
