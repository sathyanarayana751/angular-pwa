import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('en'); // Set the default language
  }

  setLanguage(lang: string): void {
    this.translate.use(lang); // Change the active language
  }

  get(key: string): string {
    return this.translate.instant(key); // Translate a given key
  }
}
