import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$ = new BehaviorSubject<User | undefined>(undefined);

  async signIn() {
    this.user$.next({
      firstName: 'XYZ',
      lastName: 'ABC',
      id: '123',
    });
  }

  async signOut() {
    this.user$.next(undefined);
  }
}
